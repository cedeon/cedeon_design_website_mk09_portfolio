const {FuseBox, VueComponentPlugin, HTMLPlugin, Sparky, WebIndexPlugin, SassPlugin, CSSResourcePlugin, CSSPlugin} = require("fuse-box");

let isProduction = false;


Sparky.task("set-prod", () => {
    isProduction = true;
});
Sparky.task("clean", () => Sparky.src("./dist").clean("dist/"));
Sparky.task("watch-assets", () => Sparky.watch("./assets", { base: "./src" }).dest("./dist"));
Sparky.task("copy-assets", () => Sparky.src("./assets", { base: "./src" }).dest("./dist"));


function oldSchool () {
    const fuse = FuseBox.init({
        homeDir: "src",
        output: "dist/$name.js",
        cache: true,
        sourceMaps: !isProduction,
        useTypescriptCompiler: true,
        polyfillNonStandardDefaultUsage: true,
        plugins: [
            VueComponentPlugin(),
            [/node_modules.*\.css$/,
                CSSResourcePlugin({inline: true}),
                CSSPlugin()
            ],
            [SassPlugin({importer: true}), CSSPlugin()],
            WebIndexPlugin({
                title: "Cedeon's Fusebox Example",
                template: "src/index.html"
            })
        ]
    });

    fuse.dev({port: 7775});

    fuse.bundle("app")
        .shim({
            jquery: {
                source: "node_modules/jquery/dist/jquery.js",
                exports: "$"
            },
            threejs: {
                source: "node_modules/three/build/three.js",
                exports: "THREE"
            }
        })
        .watch()
    // this bundle will not contain HRM related code (as only the first one gets it)
    // but we would want to HMR it
        .hmr()
        // enable sourcemaps for our package
        .sourceMaps(true)
        // bundle without deps (we have a vendor for that) + without the api
        .instructions("> index.ts");

    fuse.run();
}

Sparky.task("config", () => {
    fuse = FuseBox.init({
        homeDir: "./src",
        output: "dist/$name.js",
        //hash: isProduction,
        sourceMaps: !isProduction,
        useTypescriptCompiler: true,
        allowSyntheticDefaultImports: true,
        plugins: [
            VueComponentPlugin({
                style: [
                    SassPlugin({
                        importer: true
                    }),
                    CSSResourcePlugin(),
                    CSSPlugin({
                        //group: 'components.css',
                        //inject: 'components.css'
                    })
                ]
            }),
            [SassPlugin({importer: true}), CSSPlugin()],
            //CSSPlugin(),
            WebIndexPlugin({
                title: "Cedeon's Page",
                template: "./src/index.html"
            }),
            isProduction && QuantumPlugin({
                bakeApiIntoBundle: "vendor",
                uglify: true,
                treeshake: true
            }),
        ]
    });

    if(!isProduction){
        fuse.dev({
            //open: true,
            port: 8089
        });
    }
    
    //const vendor = fuse.bundle("vendor")
    //    .instructions("> index.ts");

    const app = fuse.bundle("app")
        .shim({
            jquery: {
                source: "node_modules/jquery/dist/jquery.js",
                exports: "$"
            },
            threejs: {
                source: "node_modules/three/build/three.js",
                exports: "THREE"
            }
        })
        .instructions("> index.ts");

    if(!isProduction){
        app.watch().hmr();
    }
})


Sparky.task("default", ["clean", "watch-assets", "config"], () => {
    return fuse.run();
});

Sparky.task("dist", [ "clean", "copy-assets", "set-prod", "config"], () => {
    return fuse.run();
});