import * as $ from "jquery";
import "./sass/index.sass";

//Vue Stuff
import Vue  from 'vue';
import Main from './components/Main.vue';

import Interactive from './js/interactive';

new Vue(Main).$mount('#app');


$(function () {
    const II = new Interactive({containerElement: '#interactive', mainElement:'#ia_canvas'});
    window.cedeonInteractive = II;  // For in browser debugging

});