/**
 * Things todo in no particular order:
 * 0) Pull out the supporting files (modules) into a separate directory
 * 1) Refactor this code so that it can take in multiple gltf files and swap them out etc.
 * 2) Multi env map support
 * 3) Implement Camera Rotation and/or lazy look at mouse pointer.
 * 4) Load in our own dioramas.
 * 5) better support for turning on and off rendering (performance?)
 * 6) Add juicy post processing effects.
 * 7) Animation support??
 * 8) Allow encapsulating within a Vue.js component?
 */

var Detector = require('./detector');
require('./GLTFLoader');
require('./orbit_controls');
import Stats from './stats'

if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

let container, stats, controls;
let camera, activeCamera , scene, renderer, light;

export default function(element) {
    init(element);
    animate();
}

function init( element ) {
    console.log('Element =' + element)
    var ell = document.querySelector(element);

    console.log(ell)

    container = document.createElement( 'div' );


    //document.body.appendChild( container );
    ell.appendChild( container );

    camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.25, 20 );
    activeCamera = camera;
    camera.position.set( -1.8, 0.9, 2.7 );
    activeCamera.aspect = window.innerWidth / window.innerHeight;
    activeCamera.updateProjectionMatrix();

    controls = new THREE.OrbitControls( activeCamera );
    controls.target.set( 0, -0.2, -0.2 );
    controls.update();

    // envmap
    var path = 'assets/img/';
    var format = '.jpg';
    var envMap = new THREE.CubeTextureLoader().load( [
        path + 'px' + format, path + 'nx' + format,
        path + 'py' + format, path + 'ny' + format,
        path + 'pz' + format, path + 'nz' + format
    ] );

    scene = new THREE.Scene();
    scene.background = envMap;

    light = new THREE.HemisphereLight( 0xbbbbff, 0x444422 );
    light.position.set( 0, 1, 0 );
    scene.add( light );

    light = new THREE.DirectionalLight( 0xffffff );
    light.position.set( -10, 6, -10 );
    scene.add( light );

    // model
    var loader = new THREE.GLTFLoader();
    //var gltfObject = 'assets/gltf/DamagedHelmet/DamagedHelmet.gltf';
    var gltfObject = 'assets/gltf/Logo/2018Logo02.gltf';

    loader.load( gltfObject, function ( gltf ) {
        console.log ('gltf object:::')
        console.log(gltf)

        gltf.scene.traverse( function ( child ) {

            if ( child.isMesh ) {

                child.material.envMap = envMap;

            }

            if (child.isCamera) {
                activeCamera = child;
                controls = new THREE.OrbitControls( activeCamera );
                controls.target.set( 0, -7, -0.01 );
                controls.update();
                activeCamera.aspect = window.innerWidth / window.innerHeight;
                activeCamera.updateProjectionMatrix();
            }

        } );

        scene.add( gltf.scene );

    } );

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.gammaOutput = true;
    container.appendChild( renderer.domElement );

    window.addEventListener( 'resize', onWindowResize, false );

    // stats
    stats = new Stats();
    container.appendChild( stats.dom );

}

function onWindowResize() {

    activeCamera.aspect = window.innerWidth / window.innerHeight;
    activeCamera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

}

//

function animate() {

    requestAnimationFrame( animate );

    renderer.render( scene, activeCamera );

    stats.update();

}